#!/usr/bin/env bash
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
sudo apt-get install apt-transport-https
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
sudo apt-get update
sudo apt-get install curl
sudo apt-get install elasticsearch

cat << EOF >/tmp/elasticsearch.yml
discovery.type: single-node
network.host: 10.0.100.170
http.port: 9200
path.data: /var/lib/elasticsearch
path.logs: /var/log/elasticsearch
EOF

sudo mv /etc/elasticsearch/elasticsearch.yml /etc/elasticsearch/elasticsearch.yml.orig
sudo mv /tmp/elasticsearch.yml /etc/elasticsearch/elasticsearch.yml

sudo update-rc.d elasticsearch defaults 95 10
sudo service elasticsearch start