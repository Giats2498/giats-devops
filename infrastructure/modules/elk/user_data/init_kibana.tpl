#!/usr/bin/env bash

wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
sudo apt-get install apt-transport-https
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
sudo apt-get update
sudo apt-get install kibana

cat << EOF >/tmp/kibana.yml
server.port: 5601
server.host: "0.0.0.0"
elasticsearch.hosts: ["http://${elasticsearch_host}:9200"]
elasticsearch.requestTimeout: 300000
elasticsearch.preserveHost: true
EOF

sudo mv /etc/kibana/kibana.yml /etc/kibana/kibana.yml.orig
sudo mv /tmp/kibana.yml /etc/kibana/kibana.yml
sudo update-rc.d kibana defaults 95 10
sudo service kibana start