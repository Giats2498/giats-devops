variable "project_name" {
  description = "Project Name"
  type        = string
  default     = null
}

variable "username" {
  description = "Database username"
  type        = string
  default     = "giatsdevops"
}

variable "database_password" {
  description = "Database Password"
  type        = string
  default     = "giatsdevops"
}

variable "allocated_storage" {
  description = "Storage Size"
  type        = number
  default     = 30
}

variable "storage_type" {
  description = "Storage Type"
  type        = string
  default     = "gp2"
}

variable "engine" {
  description = "Database Engine"
  type        = string
  default     = "postgres"
}

variable "instance_class" {
  description = "Database Class"
  type        = string
  default     = "db.t2.micro"
}

variable "snapshot_identifier" {
  description = "Restore from Database"
  type        = string
  default     = null
}

variable "option_group_name" {
  description = "Name of the DB option group to associate."
  type        = string
  default     = null
}

variable "multi_az" {
  description = "If the RDS instance is multi AZ enabled."
  type        = string
  default     = null
}

variable "availability_zone" {
  description = "Choosen AZ"
  type        = string
  default     = null
}
variable "backup_retention_period" {
  description = "The days to retain backups for. Must be between 0 and 35. Must be greater than 0 if the database is used as a source for a Read Replica. See Read Replica."
  type        = number
  default     = null
}
variable "backup_window" {
  description = "The daily time range during which automated backups are created if they are enabled. Must not overlap with maintenance_window."
  type        = string
  default     = "22:00-03:00"
}
variable "maintenance_window" {
  description = "The window to perform maintenance in."
  type        = string
  default     = "Mon:03:00-Mon:04:00"
}

variable "license_model" {
  description = "License Choice"
  type        = string
  default     = null
}

variable "storage_encrypted" {
  description = "Encrypt storage"
  type        = string
  default     = true
}
variable "copy_tags_to_snapshot" {
  description = "Copy all Instance tags to snapshots."
  type        = bool
  default     = true
}
variable "auto_minor_version_upgrade" {
  description = "Allow minor upgrades"
  type        = bool
  default     = true
}
variable "allow_major_version_upgrade" {
  description = "Allow major upgrades"
  type        = bool
  default     = false
}
variable "apply_immediately" {
  description = "Apply choices immediately"
  type        = bool
  default     = false
}

variable "public_subnet_1" {
  description = "First subnet for RDS instance"
  type        = string
  default     = null
}

variable "public_subnet_2" {
  description = "Second subnet for RDS instance"
  type        = string
  default     = null
}

variable "private_subnet_1" {
  description = "First subnet for RDS instance"
  type        = string
  default     = null
}

variable "private_subnet_2" {
  description = "Second subnet for RDS instance"
  type        = string
  default     = null
}

variable "bastion_security_group" {
  description = "Bastion host security group id"
  type        = string
  default     = null
}

variable "frontend_security_group" {
  description = "Frontend host security group id"
  type        = string
  default     = null
}

variable "public_security_group" {
  description = "Public host security group id"
  type        = string
  default     = null
}

variable "vpc_id" {
  description = "VPC ID"
  type        = string
  default     = null
}

variable "database_port" {
  description = "Database Port"
  type        = number
  default     = 5432
}
