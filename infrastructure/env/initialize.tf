provider "aws" {
  region = "us-east-1"
}

## Naming
variable "project_name" {
  description = "Project Name"
  type        = string
  default     = "giatsdevops-terraform-state"
}

variable "id_rsa_pub" {
  description = "Takes inputed public key to generate a PEM file"
  type        = string
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCxnQazwXbB4s4GowV9Q9nKbYT0T5wMKsnWTpCHuhcobsGhPq9WWts7VH4DumWkc+/7KfZbhb30dmwjgmgaG+Y2GrsAnUKZKAzqYoC/MgnAyE1KPulipJifqDi+xv0zUBOBYZvzx9F7mCIIF+i+KAE1zNNeLze6cyElvi9GaxzAsf8Ev4m/MbWWvJHf4a/X2PJU6+07C8Et8KDPd2Ksv3Tu5yCwkRSHN6kk+wcurYL2CguM3+8Z8GNGGLc1rNxrH3XAH2bnPHNui87XJA1CtnCZA4qIZZuzlZrIcKZ8AI6FcQq7oht4K1BmjjAduxMqcrjaLCaIHchsQZAAT2PtjvHrFriSj44O3SeEPvTMJseKaUeQxkpDcWHiHIHeiEdS1PZ1mFUQuK6w/bax5yxLTdceMQA0g3XZF/O2pxqYjkEJw6xD5Hltnz5iahTENnSa36VVGgW5ivXXNjC8DxPr3WEK1VnLf4DTHemuXuByeK29cXNlJX2x558PAF8UEBDVT1IkFXZrlH/Qc/zfPLlYKyipiSL0NuSZ1YQJqLYLDYmpXPlQehaPY2FT7SXQVD0h20mlX7hfIJ+UXFk55kUUdqC9Rz/I3ryGCcVM0mmdhaEFHhynPBUAzciye8l6Kk8/KpsvhyLFCtt2GsceHsokImSAaHyoFjSpOFohuHt7Cyu5qQ== vaggelisgia@hotmail.com"
}

# IAM Creation
resource "aws_iam_user" "initialize" {
  name = var.project_name
}

resource "aws_iam_user_policy_attachment" "initialize" {
  user       = aws_iam_user.initialize.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

resource "aws_iam_access_key" "initialize" {
  user = aws_iam_user.initialize.name
}

output "aws_iam_secret" {
  value = aws_iam_access_key.initialize
}

# DynamoDB

resource "aws_dynamodb_table" "initialize" {
  name           = var.project_name
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Name = var.project_name
  }
}

# S3 Bucket

# S3
resource "aws_s3_bucket" "initialize" {
  bucket        = var.project_name
  acl           = "private"
  force_destroy = true

  versioning {
    enabled = true
  }

  tags = {
    Name = var.project_name
  }
}

resource "aws_s3_bucket_policy" "initialize" {
  bucket = aws_s3_bucket.initialize.id

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "MYBUCKETPOLICY",
  "Statement": [
    {
      "Sid": "IPAllow",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "${aws_iam_user.initialize.arn}"
        ]
      },
      "Action": "s3:*",
      "Resource": "${aws_s3_bucket.initialize.arn}"
    }
  ]
}
POLICY
}

resource "aws_s3_bucket_public_access_block" "initialize" {
  bucket = aws_s3_bucket.initialize.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

# Create Iam Key Pair
resource "aws_iam_user_ssh_key" "initialize" {
  username   = aws_iam_user.initialize.name
  encoding   = "SSH"
  public_key = var.id_rsa_pub
}

resource "aws_key_pair" "initialize" {
  key_name   = "giatsdevops"
  public_key = var.id_rsa_pub
}