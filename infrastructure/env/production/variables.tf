variable "project_name" {
}
variable "domain_name" {
}
variable "email" {
}

variable "aws_region" {
}
variable "az_count" {

}

# RDS
variable "database_password" {
}
variable "allocated_storage" {
}
variable "storage_type" {
}
variable "engine" {
}
variable "instance_class" {
}
variable "backup_retention_period" {
}


# ECS
variable "key_name" {
}

variable "instance_type" {
}

variable "asg_min" {
}

variable "asg_max" {
}

variable "asg_desired" {
}

variable "service_desired" {
}

variable "compress" {
}
variable "apply_immediately" {
}
variable "storage_encrypted" {
}
variable "username" {
}
variable "database_port" {
}