terraform {
  backend "s3" {
    bucket         = "giatsdevops-terraform-state"
    encrypt        = true
    dynamodb_table = "giatsdevops-terraform-state"
    key            = "terraform.tfstate"
    region         = "us-east-1"
  }
}
