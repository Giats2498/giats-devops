# Project
project_name = "giatsdevops"
domain_name  = "giats-devops.ml"
email        = "vaggelisgia@hotmail.com"

# Global
key_name   = "giatsdevops"
aws_region = "us-east-1"
az_count   = "2"
compress   = true

# RDS
allocated_storage       = 30
storage_type            = "gp2"
engine                  = "postgres"
instance_class          = "db.t2.micro"
apply_immediately       = true
storage_encrypted       = false
username                = "giatsdevops"
database_password       = "giatsdevops"
database_port           = 5432
backup_retention_period = 7

# ECS
instance_type   = "t3.micro"
asg_min         = "1"
asg_max         = "3"
asg_desired     = "1"
service_desired = "1"