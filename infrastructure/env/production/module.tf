module "vpc" {
  source       = "../../modules/vpc"
  project_name = var.project_name
}

module "s3" {
  source       = "../../modules/s3"
  project_name = var.project_name
}

module "bastion" {
  source          = "../../modules/bastion"
  project_name    = var.project_name
  vpc_id          = module.vpc.vpc_id
  public_subnet_1 = module.vpc.public_subnet_1
  key_name        = var.key_name
}

module "acm" {
  source       = "../../modules/acm"
  project_name = var.project_name
  domain_name  = var.domain_name
}

module "route53" {
  source                 = "../../modules/route53"
  project_name           = var.project_name
  domain_name            = var.domain_name
  cloudfront_domain_name = module.cloudfront.cloudfront_domain_name
  cloudfront             = module.cloudfront.cloudfront
  aws_alb                = module.ecs.aws_alb
  bastion_public_dns     = module.bastion.bastion_public_dns
}

module "ecs" {
  source                 = "../../modules/ecs"
  project_name           = var.project_name
  aws_region             = var.aws_region
  domain_name            = var.domain_name
  min_size               = var.asg_min
  max_size               = var.asg_max
  desired_capacity       = var.asg_desired
  asg_min                = var.asg_min
  asg_max                = var.asg_max
  asg_desired            = var.asg_desired
  desired_count          = var.service_desired
  service_desired        = var.service_desired
  instance_type          = var.instance_type
  key_name               = var.key_name
  certificate            = module.acm.certificate
  private_subnet_1       = module.vpc.private_subnet_1
  private_subnet_2       = module.vpc.private_subnet_2
  public_subnet_1        = module.vpc.public_subnet_1
  public_subnet_2        = module.vpc.public_subnet_2
  vpc_id                 = module.vpc.vpc_id
  bastion_security_group = module.bastion.bastion_security_group
  kibana_public_id       = module.elk.kibana_public_id
  grafana_public_id      = module.grafana.grafana_public_id
}


module "rds" {
  source                  = "../../modules/rds"
  project_name            = var.project_name
  username                = var.username
  database_password       = var.database_password
  allocated_storage       = var.allocated_storage
  storage_type            = var.storage_type
  engine                  = var.engine
  instance_class          = var.instance_class
  apply_immediately       = var.apply_immediately
  storage_encrypted       = var.storage_encrypted
  database_port           = var.database_port
  backup_retention_period = var.backup_retention_period
  bastion_security_group  = module.bastion.bastion_security_group
  frontend_security_group = module.ecs.frontend_security_group
  private_subnet_1        = module.vpc.private_subnet_3
  private_subnet_2        = module.vpc.private_subnet_4
  vpc_id                  = module.vpc.vpc_id
}

module "cloudfront" {
  source       = "../../modules/cloudfront"
  domain_name  = var.domain_name
  project_name = var.project_name
  compress     = var.compress
  certificate  = module.acm.certificate
  media        = module.s3.s3_media_bucket_domain_name
  web_acl_cf   = aws_waf_web_acl.web_acl_cf.id
}

module "owasp_top_10" {
  source                            = "../../modules/waf"
  project_name                      = var.project_name
  frontend_environment_loadbalancer = module.ecs.loadbalancer_id
  product_domain                    = "tsi"
  service_name                      = "giatsdevops"
  environment                       = "production"
  description                       = "OWASP Top 10 rules for giats-devops.ml"
  target_scope                      = "regional"
  create_rule_group                 = "true"
  max_expected_uri_size             = "512"
  max_expected_query_string_size    = "4096"
  max_expected_body_size            = "4096"
  max_expected_cookie_size          = "4093"
  csrf_expected_header              = "x-csrf-token"
  csrf_expected_size                = "36"
}

module "owasp_top_10_global" {
  source                            = "../../modules/waf"
  project_name                      = var.project_name
  frontend_environment_loadbalancer = module.ecs.loadbalancer_id
  product_domain                    = "tsiglobal"
  service_name                      = "giatsdevops"
  environment                       = "production"
  description                       = "OWASP Top 10 rules for giats-devops.ml"
  target_scope                      = "global"
  create_rule_group                 = "true"
  max_expected_uri_size             = "512"
  max_expected_query_string_size    = "4096"
  max_expected_body_size            = "4096"
  max_expected_cookie_size          = "4093"
  csrf_expected_header              = "x-csrf-token"
  csrf_expected_size                = "36"
}

module "lambda_ami_backup_elastic_search" {
  source           = "git::https://github.com/draalin/terraform-aws-ec2-ami-backup.git"
  name             = "${var.project_name}"
  stage            = "production"
  namespace        = "elasticsearch"
  region           = "us-east-1"
  ami_owner        = "342218914711"
  instance_id      = "i-0ad7e1969b0861273"
  retention_days   = "14"
  backup_schedule  = "cron(00 19 * * ? *)"
  cleanup_schedule = "cron(05 19 * * ? *)"
}

module "elk" {
  source                      = "../../modules/elk"
  project_name                = var.project_name
  aws_region                  = var.aws_region
  private_subnet_1            = module.vpc.private_subnet_1
  private_subnet_2            = module.vpc.private_subnet_2
  public_subnet_1             = module.vpc.public_subnet_1
  public_subnet_2             = module.vpc.public_subnet_2
  vpc_id                      = module.vpc.vpc_id
  frontend_security_group     = module.ecs.frontend_security_group
  bastion_security_group      = module.bastion.bastion_security_group
  loadbalancer_security_group = module.ecs.loadbalancer_security_group
}

module "grafana" {
  source                      = "../../modules/grafana"
  project_name                = var.project_name
  aws_region                  = var.aws_region
  private_subnet_1            = module.vpc.private_subnet_1
  private_subnet_2            = module.vpc.private_subnet_2
  public_subnet_1             = module.vpc.public_subnet_1
  public_subnet_2             = module.vpc.public_subnet_2
  vpc_id                      = module.vpc.vpc_id
  frontend_security_group     = module.ecs.frontend_security_group
  bastion_security_group      = module.bastion.bastion_security_group
  loadbalancer_security_group = module.ecs.loadbalancer_security_group
}