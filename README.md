# Fullstack App Infrastructure
## Getting started
The deployment of this infrasturcture is split into two steps.

The infrastructure itself and the initialization steps of said infrastructure.
Specific resources need to be created for the infrastructure which we'll provision first.

![Infrastructure](infrastructure.svg)

## Setup
Create a ssh rsa key and add it in `infrastructure/env/initialize.tf` & `infrastructure/modules/elk/id_rsa.pub`.

Change Account ID `342218914711` in code, with your Account ID.

Copy your Your Security Credentials (Access Key ID and secret access key) and paste them to `infrastructure/env/.env.sample`

Setup Route53 with your domain name.
### Automated Setup

run `make help` to get a print out of available commands.

```
deploy                         Deploy production Terraform infrastructure
destroy                        Destroy production Terraform infrastructure
init-destroy                   Destroy Terraform initialization
init                           Create Terraform initialization
local                          Run docker-compose build locally
```

### Manual Setup
#### Initialization

```
source .env.sample
cd infrastructure/env
terraform init
terraform apply --auto-approve
```

Copy output of aws access and secret key and paste them to `infrastructure/env/production/.env.sample`

#### Infrastructure Deployment
```
cd infrastructure/env/production
```

If deploying locally update the `.env.sample` with the copied aws access and secret keys in the prior step.

To keep things simple I built out a single environment, we'll call it the `production` environment.
This can easily be duplicated by taking advantage of Terraform `workspaces`.

```
source .env.sample
terraform init
terraform workspace new production
terraform workspace select production
terraform apply --auto-approve

copy infrastructure/env/production/.env.sample variables and add them to CI/CD variables on gitlab.
git push
```

## Accessing Container Resources

### 1. Bastion
All access is done through a Bastion host. You will need the `giatsdevops.pem` file, then run:

``` bash
mv giatsdevops.pem ~/.ssh/giatsdevops.pem
chmod 600 ~/.ssh/giatsdevops.pem
echo "\nHost giatsdevops\nHostName bastion.giats-devops.ml\nUser ec2-user\nIdentityFile ~/.ssh/giatsdevops.pem" >> ~/.ssh/config
```

Then, each time you want to access the server, run:

``` bash
ssh giatsdevops
```

Alternately, ensure you have SSH Agent Forward setup as this allows you to forward your `giatsdevops.pem` onto the Bastion host as you'll need it to connect to the ECS Instances. [More info.](https://aws.amazon.com/es/blogs/security/securely-connect-to-linux-instances-running-in-a-private-amazon-vpc/)

`ssh -A -i giatsdevops.pem ec2-user@bastion.giats-devops.ml`

### 2. ECS Container Instance

Once connected to the Bastion host you can SSH into the ECS Instance

`ssh core@xxx.xxx.xxx.xxx.ec2.internal`

Notes:
* The `@ip-XXXX` address may change overtime. To find the new one go to `https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#Instances:search=giatsdevops;sort=tag:Name` and copy the private dns or ipv4.
* You may need to copy the `giatsdevops.pem` to the bastion host and connect with `ssh -i giatsdevops.pem core@xxx.xxx.xxx.xxx.ec2.internal`

#### 2.1 Accessing Containers
To access a Docker container, run the following command (replacing `api` with another container name if necessary)

``` bash
docker exec -it $(docker ps | grep 'giatsdevops-api' | awk '{print $1}') /bin/bash
```

### 3. Accessing RDS (Postgres)
To access RDS you'll need to on the Bastion host or ecs instance.

sudo amazon-linux-extras install postgresql10 -y

```
psql -h giatsdevops-production.ceajizpq43ei.us-east-1.rds.amazonaws.com -d postgres -U giatsdevops
password: XXXXXX
\l
\c postgres
\dt
SELECT * from users;
```

Troubleshooting access to RDS.

`telnet giatsdevops-production.ceajizpq43ei.us-east-1.rds.amazonaws.com 5432`

## Developer Notes

To run the project locally:

```
docker-compose up --build -d
```

Visit
```
http://localhost:4000 (WEB)
http://localhost:5000 (API)
```

##### Credits to Tyler Bailey
