const Router = require('express').Router()
const cors = require('cors')

Router.use(cors())

Router.use('/auth', require('./auth'))
Router.use('/users', require('./users'))

module.exports = Router;